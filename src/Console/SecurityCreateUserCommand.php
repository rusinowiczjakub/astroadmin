<?php

namespace App\Console;

use App\Command\User\CreateUserCommand;
use App\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityCreateUserCommand extends Command
{
    protected static $defaultName = 'security:create-user';

    private $passwordEncoder;
    private $messageBus;

    public function __construct(string $name = null,
                                UserPasswordEncoderInterface $passwordEncoder,
                                MessageBusInterface $messageBus)
    {
        parent::__construct($name);
        $this->passwordEncoder = $passwordEncoder;
        $this->messageBus = $messageBus;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates admin user.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $io->ask('Provide an email address of user');
        $password = $io->ask('Provide a password for user');

        if (empty($email) || empty($password)) {
            $io->error('You have to provide NOT EMPTY values (email and password)!');
            return Command::FAILURE;
        }

        $this->messageBus->dispatch(
            new CreateUserCommand($email, $password)
        );

        $io->success(sprintf('User %s has been created successfully', $email));

        return Command::SUCCESS;
    }
}
