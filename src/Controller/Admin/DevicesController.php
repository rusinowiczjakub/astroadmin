<?php

namespace App\Controller\Admin;

use App\Query\Device\Paginated;
use App\Repository\DeviceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DevicesController extends AbstractController
{
    /**
     * @Route("/devices", name="admin_devices")
     */
    public function index(Request $request, Paginated $paginated)
    {
        return $this->render('admin/devices/index.html.twig', [
            'pagination' => $paginated(
                $request->get('page') ?? 1,
                $request->get('limit') ?? 10
            )
        ]);
    }
}
