<?php

namespace App\Handler\User;

use App\Command\User\CreateUserCommand;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommandHandler implements MessageHandlerInterface
{
    private $repository;
    private $passwordEncoder;

    public function __construct(UserRepository $userRepository,
                                UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->repository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function __invoke(CreateUserCommand $command)
    {
        $user = new User();
        $user->setEmail($command->email)
            ->setPassword($this->passwordEncoder->encodePassword($user, $command->password))
            ->setRoles(['ROLE_ADMIN']);

        $this->repository->store($user);
    }
}