<?php

namespace App\Query\Device;

use App\Repository\DeviceRepository;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class Paginated
{
    protected DeviceRepository $repository;
    protected PaginatorInterface $paginator;

    public function __construct(DeviceRepository $deviceRepository,
                                PaginatorInterface $paginator)
    {
        $this->repository = $deviceRepository;
        $this->paginator = $paginator;
    }

    public function __invoke(?int $page = 1,
                             ?int $resultsPerPage = 10,
                             ?string $alias = 'd'): PaginationInterface
    {
        $query = $this->repository
            ->createQueryBuilder($alias)
            ->getQuery();

        return $this->paginator->paginate(
            $query,
            $page,
            $resultsPerPage
        );
    }
}