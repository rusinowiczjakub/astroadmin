<?php

namespace App\Utils\Navigation;

use App\Utils\Navigation\Enums\YamlNodesEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Yaml\Yaml;

class Main
{
    /** @var Collection<Node> */
    protected $nodes;

    public function __construct(KernelInterface $kernel)
    {
        try {
            $navigation = Yaml::parseFile(
                sprintf('%s/config/%s', $kernel->getProjectDir(), YamlNodesEnum::ADMIN_CONFIGURATION_FILE_NAME)
            );
            $this->nodes = $this->loadNodes($navigation[YamlNodesEnum::NAVIGATION]);
        } catch (\Exception $exception) {

        }
    }

    /**
     * @return Collection<Node>|null
     */
    public function getNodes(): ?Collection
    {
        return $this->nodes;
    }

    /**
     * @param array $navigation
     * @return Collection<Node>
     */
    private function loadNodes(array $navigation): ?Collection
    {
        $nodesCollection = new ArrayCollection();
        foreach ($navigation as $node) {
            $nodesCollection->add(Node::fromArray($node));
        }

        return $nodesCollection;
    }
}