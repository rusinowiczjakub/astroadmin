<?php

namespace App\Utils\Navigation;

use _HumbugBox71425477b33d\Nette\Neon\Exception;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Node
{
    /** @var string */
    protected $route;

    /** @var string */
    protected $title;

    /** @var string */
    protected $icon;

    /** @var Collection<Node> */
    protected $children = [];

    private function __construct($route, $title, $icon, $children = [])
    {
        $this->route = $route;
        $this->title = $title;
        $this->icon = $icon;
        $this->children = $children;
    }

    public static function fromArray(array $configArray)
    {
        if (isset($configArray['route']) &&
            isset($configArray['title']) &&
            isset($configArray['icon'])
        ) {
            $childNodes = new ArrayCollection();
            if (isset($configArray['children']) && !empty($configArray['children'])) {
                foreach ($configArray['children'] as $childNode) {
                    $childNodes->add(self::fromArray($childNode));
                }
            }
            return new self($configArray['route'], $configArray['title'], $configArray['icon'], $childNodes);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @return Collection
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }
}