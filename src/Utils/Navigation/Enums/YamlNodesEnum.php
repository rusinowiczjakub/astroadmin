<?php

namespace App\Utils\Navigation\Enums;

final class YamlNodesEnum
{
    public const NAVIGATION = 'navigation';
    public const ADMIN_CONFIGURATION_FILE_NAME = 'admin.yaml';
}