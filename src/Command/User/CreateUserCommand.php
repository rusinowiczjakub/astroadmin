<?php

namespace App\Command\User;

class CreateUserCommand
{
    public $email;

    public $password;

    /**
     * CreateUserCommand constructor.
     * @param $email
     * @param $password
     */
    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }
}